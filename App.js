import React from 'react';
import { Text, YellowBox } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import MainNavigator from './src/Navigator/index';
import { store, persistor } from './src/redux/store/index';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
YellowBox.ignoreWarnings([
  'Require cycle:',
]);

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(() => SplashScreen.hide(), 2000);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <MainNavigator />
        </PersistGate>
      </Provider>
    )
  }
}

export default App;