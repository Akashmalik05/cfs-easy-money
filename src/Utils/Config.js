const Live = "http://loan.cfseasymoney.com/"
const ImageUrl = "http://loan.cfseasymoney.com/public/images/"

const AllProduct = 'getall-productapi'
const Signup = 'create-user'
const Login = 'user-auth'
const ApplyNow = 'apply-loan'
const Forgotpassword = 'update-user-profile'

export default {
    AllProduct: Live + AllProduct,
    Signup: Live + Signup,
    Login: Live + Login,
    ImageUrl: ImageUrl,
    ApplyNow: Live + ApplyNow,
    Forgotpassword: Live + Forgotpassword,
}