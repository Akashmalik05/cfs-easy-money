import { Dimensions, StyleSheet } from 'react-native';
import Colors from "./Colors";

const { width, height } = Dimensions.get('window')

export const Margin = width / 35;
export const BorderRadius = Margin / 2;
export const BigTitleFontSize = height / 35;
export const TitleFontSize = height / 45;
export const ValueFontSize = height / 50;
export const BadgeFontSize = height / 80;
export const heightofIP = height / 15;
export const SideMenuImageSize = width / 7;
export const HeaderTabHeight = 55;

export const Styles = StyleSheet.create({
    //Globals
    SafeAreaViewStyle: {
        flex: 1,
        width: '100%',
        backgroundColor: Colors.BackgoundGrey
    },

    //Loading
    Loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    //Login
    ScrollViewStyle: {
        flex: 1,
        width: width,
        justifyContent: 'center',
    },
    LoginLogoStyle: {
        width: width / 2,
        height: width / 2,
    },
    LoginInputViewStyle: {
        flex: 1,
        justifyContent: 'center'
    },
    ChooseLanguageRadioBtn: {
        // flex: 1,
        flexDirection: 'row',
        width: '100%',
        marginVertical: Margin,
        marginHorizontal: -8,
    },
    ChooseLanguageTitleText: {
        fontSize: height / 45,
        color: Colors.TextGrey
    },
    AgreementViewStyle: {
        width: '100%',
        marginVertical: Margin,
        marginLeft: -8,
    },
    LoginNoteStyle: {
        backgroundColor: Colors.White,
        borderRadius: BorderRadius,
        marginTop: Margin,
        overflow: 'hidden'
    },

    //OTP
    OTPTimerText: {
        fontSize: ValueFontSize,
        color: Colors.Black,
        marginTop: Margin,
        textAlign: 'center',
    },
    OTPTitle: {
        fontSize: TitleFontSize * 1.5,
        color: Colors.Black,
        margin: Margin,
    },
    OTPDescription: {
        fontSize: TitleFontSize,
        color: Colors.TextGrey,
        textAlign: 'center',
        margin: Margin
    },
    OTPInputViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    OTPInputstyle: {
        borderWidth: height / 300,
        borderBottomWidth: height / 300,
        borderRadius: BorderRadius,
        fontSize: height / 40,
        color: Colors.Black,
    },

    //Dashboard
    DashboardMainView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    DashboardView: {
        margin: Margin,
        marginTop:0,
        backgroundColor: Colors.White,
        borderRadius: BorderRadius,
        overflow: 'hidden',
        elevation: 5,
    },
    DashboardTabMainView: {
        padding: Margin,
        flexDirection: 'row',
        alignItems: 'center',
    },
    DashboardTitleView: {
        padding: Margin,
    },

    OurProductTextStyle: {
        textAlign: 'center',
        color: Colors.TextGrey,
        fontWeight: 'bold',
        fontSize: TitleFontSize,
    },

    //GlobalInput
    InputMainView: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    InputViewStyle: {
        // width: width - (height / 15) - (Margin * 2),
    },
    InputIconChildView: {
        width: heightofIP,
        height: heightofIP,
        justifyContent: 'center',
        alignItems: 'center'
    },
    InputIconView: {
        width: (heightofIP) + 10,
        height: (heightofIP) + 2,
        backgroundColor: Colors.IconBGGrey,
        borderWidth: 1,
        borderColor: Colors.TextGrey,
        borderRadius: 2
    },
    InputStyle: {
        height: heightofIP,
        width: (width - (Margin * 2) - heightofIP),
        zIndex: 1,
        left: -10,
    },

    //Radio
    RadioTNF: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    RadioLabelStyle: {
        fontSize: ValueFontSize,
        color: Colors.TextGrey,
        marginLeft: width / 60
    },

    //Button
    ButtonStyle: {
        width: '100%',
        justifyContent: 'center',
        borderRadius: BorderRadius,
        marginTop: Margin,
        // height: heightofIP,
        bottom: 0,
    },
    ButtonLabel: {
        marginVertical: (heightofIP - ValueFontSize) / 2,
        fontSize: ValueFontSize,
    },

    //NoDataFound
    NoDataFoundTextStyle: {
        fontSize: TitleFontSize,
        color: Colors.Blue,
        fontWeight: 'bold'
    },
    NoDataFoundViewStyle: {
        flex: 1,
        justifyContent: 'center'
    },

    //Header
    HeaderMainView: {
        elevation: 2,
        backgroundColor: Colors.Blue,
        padding: Margin,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    HeaderTitleView: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: Margin
    },
    HeaderTitleStyle: {
        fontSize: TitleFontSize,
        color: Colors.White,
        textAlign: 'center'
    },

    // Tab
    TabStyle: {
        backgroundColor: Colors.Blue,
        height: HeaderTabHeight,
    },
    TabLabelStyle: {
        marginBottom: Margin / 2,
        fontSize: BadgeFontSize,
    },

    //Badge
    BadgeTextStyle: {
        fontSize: BadgeFontSize
    },

    //SideMenu
    SideMenuMainView: {
        // backgroundColor: Colors.White,
        flex: 1,
    },
    LogoutView: {
        backgroundColor: Colors.Blue,
        paddingHorizontal: Margin,
        height: 55,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    SideMenuLogoutTextStyle: {
        color: Colors.White,
        fontSize: TitleFontSize
    },
    SideMenuImage: {
        width: SideMenuImageSize,
        height: SideMenuImageSize,
        borderRadius: SideMenuImageSize,
        borderWidth: 2,
        borderColor: Colors.Blue,
        margin: Margin,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.White,
        overflow: 'hidden'
    },
    SideMenuImg: {
        width: SideMenuImageSize,
        height: SideMenuImageSize,
    },
    UserNameStyle: {
        marginBottom: Margin,
        // textAlign: 'center',
        fontSize: ValueFontSize,
        paddingHorizontal: Margin,
        color: Colors.TextBlack,
    },
    SideMenuTabs: {
        padding: Margin,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: Colors.IconBGGrey,
        borderBottomWidth: StyleSheet.hairlineWidth * 2
    },
    SideMenuTabsTextStyle: {
        color: Colors.TextGrey,
        fontSize: TitleFontSize,
    },

    LogoViewStyle: {
        alignItems: 'center',
        margin: Margin,
    },

    LogoViewStyle2: {
        borderRadius: height / 2,
        elevation: 5,
        overflow: 'hidden',
        margin: Margin,
    },

    NoteMargin: {
        marginHorizontal: Margin,
    },
    RegisterasText:
    {
        color: Colors.Black,
        fontSize: ValueFontSize,
    },
})