import TextJson from "./TextJson";
import { Dimensions } from "react-native";

const { width, height } = Dimensions.get('window')

export default {
    ChooseLanguage: [{
        title: TextJson.English,
        code: 'en',
        checked: false,
    }, {
        title: TextJson.Hindi,
        code: 'hi',
        checked: false,
    }],
    DashboardItems: {
        Banners: [{
            image: require('../Component/assets/images/Home-page-carousel/01.png')
        },
        {
            image: require('../Component/assets/images/Home-page-carousel/02.png')
        },
        {
            image: require('../Component/assets/images/Home-page-carousel/03.png')
        },
        {
            image: require('../Component/assets/images/Home-page-carousel/04.png')
        }]
    },
    SideMenuTabs: [
        {
            title: TextJson.Home1,
            Image: require('../Component/assets/Menu-page/Home.png'),
            screen: 'Home',
        },
        {
            title: TextJson.AboutUs,
            Image: require('../Component/assets/Menu-page/About.png'),
            screen: 'AboutUs',
        },
        {
            title: TextJson.OurProduct,
            Image: require('../Component/assets/Menu-page/Our-Products.png'),
            screen: 'OurProduct',
        },
        {
            title: TextJson.ApplyNow,
            Image: require('../Component/assets/Menu-page/Apply-Now.png'),
            screen: 'OurProduct',
        },
        {
            title: TextJson.Login,
            Image: require('../Component/assets/Menu-page/Login.png'),
            screen: 'UserIdentification',
        },
        {
            title: TextJson.Logout,
            Image: require('../Component/assets/Menu-page/Logout.png'),
            screen: 'UserIdentification',
        }
    ],
}