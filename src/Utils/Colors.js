export default {
    Blue: 'rgba(177, 5, 1, 1)',
    LightBlue: 'rgba(246, 107, 86,1)',
    MidBlue: 'rgb(226, 71, 49)',
    White: 'rgba(255,255,255,255)',
    TextBlack: 'rgb(4, 4, 3)',
    Black: 'rgba(0,0,0,1)',
    TextGrey: 'rgba(114, 109, 106,1)',
    IconBGGrey: 'rgba(73,80,87,0.2)',
    BackgoundGrey: 'rgba(255,248,246,1)',
    Red: 'rgb(255, 0, 0)',
    Trans: 'rgba(0,0,0,0)',
}