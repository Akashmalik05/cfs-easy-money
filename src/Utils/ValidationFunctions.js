import TextJson from "./TextJson";
import { GetApi, PutApi, PostApi } from "../Services/Api";
import Config from "./Config";
import { StackActions } from "@react-navigation/native";

export const checkDefaultLanguage = (array = [], code = 'en') => {
  if (array.length > 0) {
    array.map((item, index) => {
      if (item.code == code) {
        item.checked = true
      }
      else {
        item.checked = false
      }
    })
    return { data: array, code: code };
  }
}

export const SubmitLogin = async (MobileNo, Password, props) => {
  const { language } = props.app;
  props.loginstart()
  console.log("Pfffffffffffffff", props)
  return PostApi(Config.Login, {
    "user_name": MobileNo,
    "password": Password
  })
    .then((response) => {
      console.log("response", response)
      return response;
    })
    .catch((error) => {
      console.log("error", error)
      props.loginstop({})
      return;
    })
}

export const SubmitSignup = async (body, props) => {
  const { language } = props.app;
  props.signupstart()
  return PostApi(Config.Signup, body)
    .then((response) => {
      console.log("response", response)
      return response;
    })
    .catch((error) => {
      console.log("error", error)
      props.signupstop({})
    })
  return;
}

export const SubmitApplyNow = async (body, props) => {
  const { language } = props.app;
  return PostApi(Config.ApplyNow, body)
    .then((response) => {
      console.log("response", response)
      return response;
    })
    .catch((error) => {
      console.log("error", error)
      return;
    })
}


export const ForgotPassword = async (MobileNo, Password, props) => {
  return PostApi(Config.Forgotpassword, {
    "mobile": MobileNo,
    "password": Password
  })
    .then((response) => {
      console.log("response", response)
      return response;
    })
    .catch((error) => {
      console.log("error", error)
    })
  return;
}