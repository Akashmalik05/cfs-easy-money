import Type from '../action/index'

export function IncrDecr(dispatch) {
  return {
    loginstart: () => dispatch({ type: Type.LoginStart }),
    loginstop: (data) => dispatch({ type: Type.LoginStop, data: data }),
    signupstart: () => dispatch({ type: Type.SignupStart }),
    signupstop: (data) => dispatch({ type: Type.SignupStop, data: data }),
    storeMobile: (data) => dispatch({ type: Type.StoreMobile, data: data }),
    logout: () => dispatch({ type: Type.Logout }),
    homeContentStart: () => dispatch({ type: Type.HomeContentStart }),
    homeContentStop: (data) => dispatch({ type: Type.HomeContentStop, data: data }),
  }
}

export function loginprops(state) {
  return {
    app: state
  };
}