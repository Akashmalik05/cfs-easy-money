const LoginStart = 'LOGINSTART'
const LoginStop = 'LOGINSTOP'
const SignupStart = 'SIGNUPSTART'
const SignupStop = 'SIGNUPSTOP'
const Logout = 'LOGOUT'
const StoreMobile = 'STOREMOBILE'
const ERROR = 'ERROR'
const HomeContentStart = 'HOMECONTENTSTART'
const HomeContentStop = 'HOMECONTENTSTOP'

export default {
    LoginStart: LoginStart,
    LoginStop: LoginStop,
    Logout: Logout,
    StoreMobile: StoreMobile,
    ERROR: ERROR,
    SignupStart: SignupStart,
    SignupStop: SignupStop,
    HomeContentStart: HomeContentStart,
    HomeContentStop: HomeContentStop
}