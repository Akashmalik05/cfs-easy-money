import { combineReducers } from 'redux'
import Type from '../action/index'
const LoginData = {
    loginStatus: false,
    loading: false
}
const SignupData = {
    loading: false
}
const language = {
    languagecode: 'en',
    loading: false
}
const OnlineStatus = {
    isOnline: true,
    loading: false
}

const HomeContent = {
    data: [],
    loading: false
}

const reducerForLogin = (state = LoginData, action) => {
    switch (action.type) {
        case Type.LoginStart:
            return {
                ...state, loading: true
            }
        case Type.LoginStop:
            return {
                ...state, ...action.data, loading: false, loginStatus: action.data && action.data.status && action.data.status == 'ok' ? true : false
            }
        case Type.Logout:
            return LoginData;
    }
    return state;
}

const reducerForSignup = (state = SignupData, action) => {
    switch (action.type) {
        case Type.SignupStart:
            return {
                ...state, loading: true
            }
        case Type.SignupStop:
            return {
                ...state, ...action.data, loading: false
            }
    }
    return state;
}

const reducerForLanguage = (state = language, action) => {
    switch (action.type) {
        case Type.Language:
            return { ...state, languagecode: action.code };
    }
    return state;
}

const reducerForOnOffline = (state = OnlineStatus, action) => {
    switch (action.type) {
        case Type.Online:
            return { ...state, isOnline: true };
        case Type.Offline:
            return { ...state, isOnline: false };
    }
    return state;
}

const reducerForHomeContent = (state = HomeContent, action) => {
    switch (action.type) {
        case Type.HomeContentStart:
            return { ...state, loading: true };
        case Type.HomeContentStop:
            return { data: action.data, loading: false };
    }
    return state;
}

export const allreducer = combineReducers({
    login: reducerForLogin,
    homeContent: reducerForHomeContent,
    language: reducerForLanguage,
    OnOffline: reducerForOnOffline,
    Signup: reducerForSignup,
})