import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Dimensions,
  FlatList,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { TouchableNativeFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import { IncrDecr, loginprops } from '../../redux/dispatcher/index';
import Colors from '../../Utils/Colors';
import { Styles, Margin } from '../../Utils/Styles';
import ArrayJson from '../../Utils/ArrayJson';
import NoDataFound from '../../Component/Globals/NoDataFound';
import { StackActions } from '@react-navigation/native';
import { ActivityIndicator } from 'react-native-paper';
import Buttons from '../../Component/Globals/Buttons';
import TextJson from '../../Utils/TextJson';
import { SetOnlineOffline } from '../../Utils/ValidationFunctions';
import { GetApi } from '../../Services/Api';
import Config from '../../Utils/Config';
import Loading from '../../Component/Globals/Loading';

const { width, height } = Dimensions.get('window')

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ListData: [],
      isOnline: true,
    }
  }

  async componentDidMount() {
    const { login } = this.props.app
    await GetApi(Config.HomeContent, this.props)
      .then((response) => {
        console.log("Resss Home", response)
        this.setState({ ListData: response, loading: false })
      })
    // this.setState({ ListData: ArrayJson.DashboardItems[login.usertype] })
  }

  renderItem = ({ item, index }) => {
    const { ListData } = this.state
    const { language } = this.props.app
    return (
      <View style={[Styles.DashboardTabMainView, { marginTop: index < 2 ? Margin : Margin / 2, marginBottom: index > ListData.length - 3 ? Margin : Margin / 2 }]}>
        <TouchableNativeFeedback style={[Styles.DashboardTouchableView]}>
          <Icon
            name={item.name}
            type={item.type}
            color={Colors.Black}
            size={item.size}
          />
          <View style={Styles.DashboardTitleView}>
            <Text style={{ textAlign: 'center', color: Colors.Black }}>{item.title[language.languagecode]}</Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    )
  }

  render() {
    console.log(this.props)
    const { loading, ListData, isOnline } = this.state
    const { language, OnOffline, login } = this.props.app
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {loading ?
          <Loading />
          :
          <View style={Styles.DashboardMainView}>
            {ListData && ListData.length > 0 ?
              <FlatList
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                numColumns={2}
                style={{ width: '100%' }}
                data={ListData}
                extraData={this.state}
                keyExtractor={(item, index) => String(index)}
                renderItem={this.renderItem}
              />
              : <NoDataFound
                language={language}
              />
            } 
            {login.groupName == 'Supplier' ? <View style={{ width: '100%', justifyContent: 'space-evenly', flexDirection: 'row', margin: Margin, marginTop: 0 }}>
              <Buttons
                Width={(width - (Margin * 2))}
                Label={OnOffline.isOnline ? TextJson.Offline[language.languagecode] : TextJson.Online[language.languagecode]}
                OnPress={async () => { await SetOnlineOffline(this.props); this.setState({}) }}
              />
            </View> : null}
          </View>
        }
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(Home);