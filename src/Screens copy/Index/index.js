import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../redux/dispatcher/index';
import { StackActions } from '@react-navigation/native';
import { Image } from 'react-native-elements';
import Colors from '../../Utils/Colors';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    console.log("a", this.props.app.login)
    if (this.props.app.login.loginStatus) {
      this.props.navigation.dispatch(
        StackActions.replace('DrawerApp')
      );
    }
    else {
      this.props.navigation.dispatch(
        StackActions.replace('UserIdentificationStack')
      );
    }
  }

  render() {
    return (
      <View style={{ flex: 1, width: '100%', backgroundColor: 'red' }}>
        <Image
          placeholderStyle={{ backgroundColor: Colors.Trans }}
          source={require('../../Component/assets/images/launch_screen.jpg')}
          style={{ width: '100%', height: '100%' }}
          resizeMode='cover'
        />
      </View>
    )
  }
}

export default connect(loginprops, IncrDecr)(Index);