import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  KeyboardAvoidingView,
  Dimensions,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../../redux/dispatcher/index';
import { Styles, Margin } from '../../../../Utils/Styles';
import * as Function from '../../../../Utils/ValidationFunctions';
import LoginNote from "../../../../Component/Globals/LoginNote";
import TextJson from '../../../../Utils/TextJson';
// import OTPTextInput from "react-native-otp-textinput";
import Colors from '../../../../Utils/Colors';
import Buttons from '../../../../Component/Globals/Buttons';
// import RNOtpVerify from 'react-native-otp-verify';
import Loading from '../../../../Component/Globals/Loading';

const { width, height } = Dimensions.get('window')

{/* <ActivityIndicator size={height/25} color={Colors.Blue}/> */ }
class AuthenticationCode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      OTP: '',
      timer: 10,
      ResendOTP: false
    }
  }

  componentDidMount() {
    this.setState({ loading: false })
    // this.startTimer()
    // this.getHash()
    // this.startListeningForOtp()
  }

  // getHash = () =>
  //   RNOtpVerify.getHash()
  //     .then((res) => console.log("getHash", res))
  //     .catch(console.log);

  // startListeningForOtp = () =>
  //   RNOtpVerify.getOtp()
  //     .then(p => {
  //       console.log("startListeningForOtp", p);
  //       RNOtpVerify.addListener(this.otpHandler)
  //     })
  //     .catch(p => console.log(p));

  // otpHandler = (message) => {
  //   if (message) {
  //     const otp = /(\d{4})/g.exec(message)[1];
  //     console.log("otp", otp)
  //     this.otpInput.setValue(otp)
  //     this.setState({ OTP: otp, timer: 0 });
  //   }
  //   RNOtpVerify.removeListener();
  //   Keyboard.dismiss();
  // }

  // componentWillUnmount() {
  //   RNOtpVerify.removeListener();
  // }

  // startTimer(newtime) {
  //   const { timer } = this.state;
  //   let time = newtime ? newtime : timer - 1
  //   setTimeout(() => {
  //     this.setState({ timer: time })
  //     if (time <= 0) {
  //       this.setState({ ResendOTP: true })
  //       return;
  //     }
  //     else {
  //       this.startTimer()
  //     }
  //   }, 1000);
  // }

  // ResendOTP() {
  //   this.setState({ timer: 10, ResendOTP: false })
  //   this.startTimer(9);
  // }

  // async Submit() {
  //   const { OTP } = this.state;
  //   this.setState({ loading: true })
  //   await Function.VerifyOTP(OTP, this.props)
  //   this.setState({ loading: false })
  // }

  render() {
    const { timer, ResendOTP, loading } = this.state
    const { language, login } = this.props.app
    console.log("loginloginlogin", login)
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {loading ?
          <Loading />
          :
          <KeyboardAvoidingView style={Styles.ScrollViewStyle}>
            {/* <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}>
              <View style={Styles.NoteMargin}>
                <LoginNote
                  Content={`${TextJson.OTPNote1[language.languagecode]} ${login.MobileNo} ${TextJson.OTPNote2[language.languagecode]} ${login.otp}`}
                />
              </View>
            </View>

            <View style={Styles.OTPInputViewStyle}>
              <OTPTextInput
                ref={e => (this.otpInput = e)}
                textInputStyle={Styles.OTPInputstyle}
                tintColor={Colors.Blue}
                offTintColor={Colors.Blue}
                handleTextChange={(otp) => this.setState({ OTP: otp })}
              />
              <Text style={Styles.OTPTimerText}>{timer == 60 ? '01:00' : timer > 9 ? `00:${timer}` : `00:0${timer}`}{ResendOTP ? <Text style={{ color: Colors.Blue }}>     <Text style={{ textDecorationLine: 'underline' }} onPress={() => this.ResendOTP()}>{TextJson.ResendOTP[language.languagecode]}</Text></Text> : null}</Text>
            </View>
            <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}>

              <View style={Styles.NoteMargin}>
                <LoginNote
                  HideColor={true}
                  Content={TextJson.OTPNote3[language.languagecode]}
                />
              </View>
            </View>
            <View style={{ marginHorizontal: Margin }}>
              <Buttons
                Label={TextJson.Verify[language.languagecode]}
                OnPress={() => this.Submit()}
                MarginBottom={Margin}
              />
            </View> */}
          </KeyboardAvoidingView>}
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(AuthenticationCode);