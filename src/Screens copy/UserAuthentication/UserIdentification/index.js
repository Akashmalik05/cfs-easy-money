import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  Dimensions,
  TextInput,
  Animated
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Styles, Margin } from '../../../Utils/Styles';
import Inputs from '../../../Component/Globals/Inputs';
import RadioButtons from '../../../Component/Globals/RadioButtons';
import ArrayJson from '../../../Utils/ArrayJson';
import TextJson from '../../../Utils/TextJson';
import Buttons from '../../../Component/Globals/Buttons';
import * as Function from '../../../Utils/ValidationFunctions';
import Colors from '../../../Utils/Colors';
import LoginNote from '../../../Component/Globals/LoginNote';
import Logo from '../../../Component/Globals/Logo';
import { PostApi, GetApi } from '../../../Services/Api';
import Config from '../../../Utils/Config';
import ActivityIndicator from 'react-native-paper/lib/module/components/ActivityIndicator';
import Loading from '../../../Component/Globals/Loading'

const { width, height } = Dimensions.get('window')

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      Mobileno: '',
      isCheckedAgreement: false,
    }
  }

  async componentDidMount() {
    let languagedata = await Function.checkDefaultLanguage(ArrayJson.ChooseLanguage, this.props.app.language.languagecode)
    this.setState({ LanguageArray: languagedata.data, LanguageCode: languagedata.code, loading: false })
  }

  async Submit(MobileNo) {
    this.setState({ loading: true })
    await Function.SubmitLogin(MobileNo, this.props)
    this.props.navigation.navigate("AuthenticationCode")
    this.setState({ loading: false })
  }

  ChooseLang(item, index) {
    const { LanguageArray } = this.state
    const { language } = this.props.app;
    let Result = Function.CheckLanguage(LanguageArray, index)
    if (Result) {
      this.props.language(Result)
      this.setState({ LanguageCode: Result })
    }
  }

  render() {
    const { loading, Mobileno, LanguageArray, LanguageCode, isCheckedAgreement } = this.state
    const { language } = this.props.app
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {loading ?
          <Loading />
          :
          <KeyboardAvoidingView style={Styles.ScrollViewStyle}>
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
              <Logo />
              <View style={{ flex: 1, paddingHorizontal: Margin }}>
                <View style={{ width: '100%' }}>
                  <Text style={Styles.ChooseLanguageTitleText}>{TextJson.ChooseLanguage[LanguageCode]}</Text>
                  <View style={Styles.ChooseLanguageRadioBtn}>
                    {LanguageArray ? LanguageArray.map((item, index) => {
                      return (
                        <View style={{ flex: 1 }} key={index}>
                          <RadioButtons
                            Type='Radio'
                            Label={item.title[LanguageCode]}
                            Condition={item.checked}
                            OnPress={() => this.ChooseLang(item, index)}
                          />
                        </View>
                      )
                    }) : null}
                  </View>
                </View>
                <View style={Styles.LoginInputViewStyle}>
                  <Inputs
                    Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                    Label={TextJson.MobileNo[LanguageCode]}
                    KeyboardType='number-pad'
                    MaxLength={10}
                    Value={Mobileno}
                    OnChangeText={Mobileno => this.setState({ Mobileno: Mobileno })}
                  />
                </View>
                <LoginNote
                  Content={TextJson.LoginNote[LanguageCode]}
                />
                <View style={Styles.AgreementViewStyle}>
                  <RadioButtons
                    Label={LanguageCode == 'en' ? <Text>{TextJson.IAgreement[LanguageCode]} {TextJson.Agreement[LanguageCode]} <Text style={{ color: Colors.Blue, textDecorationLine: 'underline' }}>{TextJson.TermsConsitions[LanguageCode]}</Text></Text> : <Text>{TextJson.IAgreement[LanguageCode]} <Text style={{ color: Colors.Blue, textDecorationLine: 'underline' }}>{TextJson.TermsConsitions[LanguageCode]}</Text> {TextJson.Agreement[LanguageCode]}</Text>}
                    Condition={isCheckedAgreement}
                    OnPress={() => { this.setState({ isCheckedAgreement: !isCheckedAgreement }) }}
                  />
                </View>
              </View>
            </ScrollView>
            <View style={{ marginHorizontal: Margin }}>
              <Buttons
                Disabled={isCheckedAgreement ? false : true}
                Label={TextJson.Continue[LanguageCode]}
                OnPress={() => this.Submit(Mobileno)}
                MarginBottom={Margin}
              />
            </View>
          </KeyboardAvoidingView>}
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(Home);

















// import React from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
// } from 'react-native';
// import {connect} from 'react-redux';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import {IncrDecr, loginprops} from '../../redux/dispatcher/index';
// import { StackActions } from '@react-navigation/native';

// class Login extends React.Component{
//   constructor(props) {
//       super(props);
//       this.state = {
//       }
//   }

//   render()
//   {
//     return(
//       <View style={{alignItems:'center', justifyContent:'center', flex:1, width:'100%'}}>
//           <Text style={{fontSize:50}} onPress={()=>this.props.navigation.setParams({})}>Login Screen</Text>
//           <TouchableOpacity style={{backgroundColor:'grey', width:'100%', marginBottom:10}} onPress={()=>{this.props.login('8979867545');this.props.navigation.dispatch(
//         StackActions.replace('DrawerApp')
//       );}}>
//             <Text>login</Text>
//           </TouchableOpacity>
//       </View>
//     )
//   }
// }

// export default connect(loginprops, IncrDecr)(Login);