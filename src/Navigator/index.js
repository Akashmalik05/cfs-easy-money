import * as React from 'react';
import { Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Header from '../Component/Globals/Header';
import { IncrDecr, loginprops } from '../redux/dispatcher/index';


//screen
import Index from '../Screens/Index/index';
import Home from '../Screens/Home/index';
import OurProduct from '../Screens/Home/OurProduct/index';
import Details from '../Screens/Home/Details/index';
import ApplyNow from '../Screens/Home/Details/ApplyNow';
import AboutUs from '../Screens/Home/AboutUs/index';
import UserIdentification from '../Screens/UserAuthentication/UserIdentification/index';
import Signup from '../Screens/UserAuthentication/UserApplication/index';
import SideMenu from '../Component/Globals/SideMenu';
import { connect } from 'react-redux';
import TextJson from '../Utils/TextJson';
import CarLoanView from '../Screens/Home/CarLoanView';
import CreditCardView from '../Screens/Home/CreditCardView';
import ForgotPassword from '../Screens/UserAuthentication/UserIdentification/ForgotPassword';
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const { width, height } = Dimensions.get('window')

const showDrawer = false;

function homestack({ app }) {
    const { languagecode } = app.language
    return (
        <Stack.Navigator initialRouteName='Home'>
            <Stack.Screen name="Home" component={Home} options={{ header: props => <Header Title={TextJson.Home[languagecode]} RightIcon={true} OnMenuPress={() => props.navigation.toggleDrawer()} /> }} />
            <Stack.Screen name="OurProduct" component={OurProduct} options={{ header: props => <Header Title={TextJson.OurProduct[languagecode]} Type="Back" RightIcon={true} OnMenuPress={() => props.navigation.goBack(null)} /> }} />
            <Stack.Screen name="Details" component={Details} options={{ header: props => <Header Title={TextJson.Details[languagecode]} Type="Back" RightIcon={true} OnMenuPress={() => props.navigation.goBack(null)} /> }} />
            <Stack.Screen name="ApplyNow" component={ApplyNow} options={{ header: props => <Header Title={TextJson.ApplyNow[languagecode]} Type="Back" RightIcon={true} OnMenuPress={() => props.navigation.goBack(null)} /> }} />
            <Stack.Screen name="AboutUs" component={AboutUs} options={{ header: props => <Header Title={TextJson.AboutUs[languagecode]} RightIcon={true} OnMenuPress={() => props.navigation.toggleDrawer()} /> }} />
            <Stack.Screen name="CarLoanView" component={CarLoanView} options={{ header: props => <Header Title={TextJson.CarLoan[languagecode]} RightIcon={true} OnMenuPress={() => props.navigation.toggleDrawer()} /> }} />
            <Stack.Screen name="CreditCardView" component={CreditCardView} options={{ header: props => <Header Title={TextJson.CreditCard[languagecode]} RightIcon={true} OnMenuPress={() => props.navigation.toggleDrawer()} /> }} />
            <Stack.Screen name="UserIdentification" component={UserIdentification} options={{ header: props => <Header Title={TextJson.Login[languagecode]} Type="Back" RightIcon={true} OnMenuPress={() => props.navigation.goBack(null)} /> }} />
            <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{ header: props => <Header Title={TextJson.ForgotPassword[languagecode]} Type="Back" RightIcon={true} OnMenuPress={() => props.navigation.goBack(null)} /> }} />
            <Stack.Screen name="Signup" component={Signup} options={{ header: props => <Header Title={TextJson.Signup[languagecode]} Type="Back" RightIcon={true} OnMenuPress={() => props.navigation.goBack(null)} /> }} />
        </Stack.Navigator>
    )
};
const HomeStack = connect(loginprops, IncrDecr)(homestack);

function DrawerApp() {
    return (
        <Drawer.Navigator
            drawerStyle={{
                backgroundColor: 'rgba(255,255,255,0.85)',
            }}
            drawerContent={props => <SideMenu {...props} />}
        >
            <Drawer.Screen name="HomeStack" component={HomeStack} />
        </Drawer.Navigator>
    );
}

function indexStack({ navigation, route, app }) {
    const { languagecode } = app.language
    return (
        <Stack.Navigator>
            <Stack.Screen name="Index" component={Index} options={{ headerShown: false }} />
            <Stack.Screen name="DrawerApp" component={DrawerApp} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
};
const IndexStack = connect(loginprops, IncrDecr)(indexStack);
export default function MainNavigator() {
    return (
        <NavigationContainer>
            <IndexStack />
        </NavigationContainer>
    );
}