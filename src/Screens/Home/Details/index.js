import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import Colors from '../../../Utils/Colors';
import { Styles, Margin, BorderRadius, BigTitleFontSize, ValueFontSize } from '../../../Utils/Styles';
import TextJson from '../../../Utils/TextJson';
import Config from '../../../Utils/Config';
import Image from 'react-native-scalable-image';
import Buttons from '../../../Component/Globals/Buttons';
import HTML from "react-native-render-html";
const { width } = Dimensions.get('window')

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Details: {},
      detail: ""
    }
  }

  async componentDidMount() {
    if (this.props && this.props.route && this.props.route.params && this.props.route.params.item) {
      let det = this.props.route.params.item.product_description.split('&lt;')
      det = det.join('<')
      det = det.split('&gt;')
      det = det.join('>')
      det = det.split('&quot;')
      det = det.join('')
      det = det.split('&nbsp;')
      det = det.join('')
      console.log("det", det)
      this.setState({ Details: this.props.route.params.item, detail: det })
    }
  }

  CheckLogin() {
    const { login } = this.props.app
    const { Details } = this.state
    if (login.loginStatus) {
      this.props.navigation.navigate('ApplyNow', { LoanType: Details.product_name })
    }
    else {
      Alert.alert(
        "",
        "To see details, you need to login first.",
        [
          {
            text: "Cancel",
            style: "cancel"
          },
          { text: "Login", onPress: () => this.props.navigation.navigate('UserIdentification', { Type: "Apply", LoanType: Details.product_name }) }
        ],
        { cancelable: false }
      );
    }
  }

  render() {
    const { Details, detail } = this.state
    const { language } = this.props.app
    console.log("DetailsDetailsDetails", Details)
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {Details && Details.id ? <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ margin: Margin }}>
            <Text style={{ color: Colors.TextBlack, textAlign: 'center', fontWeight: 'bold', fontSize: BigTitleFontSize }}>{Details.product_name}</Text>
          </View>
          <View style={{ alignItems: 'center', marginHorizontal: Margin, borderRadius: BorderRadius, elevation: 5, overflow: 'hidden' }}>
            <Image
              width={width - (Margin * 2)}
              source={{ uri: Config.ImageUrl + Details.product_banner }}
              resizeMode='cover'
            />
          </View>
          <View style={{ margin: Margin }}>
            <HTML source={{ html: detail }} />
          </View>
        </ScrollView> : null}
        <View style={{ marginHorizontal: Margin }}>
          <Buttons
            Label={TextJson.ApplyNow[language.languagecode]}
            OnPress={() => this.CheckLogin()}
            MarginBottom={Margin}
          />
        </View>
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(Details);