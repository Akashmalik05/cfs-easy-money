import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    View,
    KeyboardAvoidingView,
    Dimensions,
    Platform,
    UIManager,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import { Icon } from 'react-native-elements';
import { Styles, Margin, heightofIP } from '../../../Utils/Styles';
import Inputs from '../../../Component/Globals/Inputs';
import ArrayJson from '../../../Utils/ArrayJson';
import TextJson from '../../../Utils/TextJson';
import Buttons from '../../../Component/Globals/Buttons';
import * as Function from '../../../Utils/ValidationFunctions';
import Colors from '../../../Utils/Colors';
import Loading from '../../../Component/Globals/Loading'

const { width, height } = Dimensions.get('window')

class ApplyNow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            Mobileno: '',
            City: '',
            CityError: '',
            MobileError: '',
            State: '',
            StateError: '',
            Email: '',
            EmailError: '',
            Name: '',
            NameError: '',
            Pincode: '',
            PincodeError: '',
            LoanAmount: '',
            LoanAmountError: '',
            LoanPeriod: '',
            LoanPeriodError: '',
            Address: '',
            AddressError: '',
            PanNumber: '',
            PanNumberError: '',
            LoanType: '',
            LoanTypeError: '',
            isCheckedAgreement: false,
        }
    }

    async componentDidMount() {
        const { login } = this.props.app;
        // console.log("DisableEditableDisableEditableDisableEditable", this.props.route.params)
        if (this.props && this.props.route && this.props.route.params && this.props.route.params.LoanType) {
            this.setState({ LoanType: this.props.route.params.LoanType })
        }
        let languagedata = await Function.checkDefaultLanguage(ArrayJson.ChooseLanguage, this.props.app.language.languagecode)
        this.setState({ LanguageArray: languagedata.data, LanguageCode: languagedata.code, Name: login.result.name, Email: login.result.email, Mobileno: login.result.phone_no })
        if (Platform.OS == 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    async Submit(MobileNo) {
        Keyboard.dismiss()
        const {
            Mobileno,
            Email,
            Name,
            Address,
            City,
            State,
            Pincode,
            PanNumber,
            LoanAmount,
            LoanPeriod,
            LoanType
        } = this.state
        let regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regexMobile = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        let regexPincode = /^(\d{4}|\d{6})$/
        let regexPancard = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/
        if (Name == '') {
            this.setState({ NameError: "Please enter Name.", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (Email == '') {
            this.setState({ NameError: "", EmailError: 'Please enter Email.', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (!regexEmail.test(Email)) {
            this.setState({ NameError: "", EmailError: 'Please enter valid Email.', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (Mobileno == '') {
            alert(Mobileno)
            this.setState({ NameError: "", EmailError: '', MobileError: 'Please enter Mobile number.', AddressError: '', CityError: '', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (!regexMobile.test(Mobileno)) {
            this.setState({ NameError: "", EmailError: '', MobileError: 'Please enter valid Mobile number.', AddressError: '', CityError: '', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (Address == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: 'Please enter Address.', CityError: '', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (City == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: 'Please enter City.', StateError: '', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (State == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: 'Please enter State.', PincodeError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (Pincode == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: 'Please enter Pincode.', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (!regexPincode.test(Pincode)) {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: 'Please enter valid Pincode.', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '', PanNumberError: '' })
        }
        else if (PanNumber == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', PanNumberError: 'Please enter PanNumber.', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '' })
        }
        else if (!regexPancard.test(PanNumber)) {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', PanNumberError: 'Please enter valid PanNumber.', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '' })
        }
        else if (LoanAmount == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', PanNumberError: '', LoanAmountError: 'Please enter Loan Amount.', LoanPeriodError: '', LoanTypeError: '' })
        }
        else if (LoanPeriod == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', PanNumberError: '', LoanAmountError: '', LoanPeriodError: 'Please enter Loan Period.', LoanTypeError: '' })
        }
        else if (LoanType == '') {
            this.setState({ NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', PanNumberError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: 'Please enter Loan Type.' })
        }
        else {
            this.setState({ loading: true, NameError: "", EmailError: '', MobileError: '', AddressError: '', CityError: '', StateError: '', PincodeError: '', PanNumberError: '', LoanAmountError: '', LoanPeriodError: '', LoanTypeError: '' })
            let body = {
                "name": Name,
                "email": Email,
                "phone": MobileNo,
                "city": City,
                "state": State,
                "pincode": Pincode,
                "loan_amount": LoanAmount,
                "loan_period": LoanPeriod,
                "address": Address,
                "pan_number": PanNumber.toUpperCase(),
                "loan_type": LoanType,
            }
            let response = await Function.SubmitApplyNow(body, this.props)
            console.log("LoanPeriodLoanPeriod", response)
            if (response) {
                if (response.status == 'error') {
                    this.setState({ loading: false })
                    alert(response.result)
                }
                else if (response.status == 'ok') {
                    this.setState({ loading: false })
                    alert("You have successfully applied for loan.")
                    this.props.navigation.goBack(null)
                }
                else {
                    this.setState({ loading: false })
                    console.log("err", response)
                }
            }
            else {
                this.setState({ loading: false })
            }
        }
    }

    CheckButtonStatus(type, Text) {
        if (type == 'Pincode') {
            let text = Text.trim()
            if (text != '' && isNaN(text[text.length - 1])) {
            }
            else {
                this.setState({ Pincode: text.trim() })
            }
        }
        else if (type == 'LoanAmount') {
            let text = Text.trim()
            if (text != '' && isNaN(text[text.length - 1])) {
            }
            else {
                this.setState({ LoanAmount: text.trim() })
            }
        }
        else if (type == 'LoanPeriod') {
            let text = Text.trim()
            this.setState({ LoanPeriod: text.trim() })
        }
        else if (type == 'LoanType') {
            let text = Text.trim()
            if (text != '' && isNaN(text[text.length - 1])) {
            }
            else {
                this.setState({ LoanType: text.trim() })
            }
        }
    }

    render() {
        const {
            Mobileno,
            City,
            CityError,
            MobileError,
            State,
            StateError,
            Email,
            EmailError,
            Name,
            NameError,
            Pincode,
            PincodeError,
            LoanAmount,
            LoanAmountError,
            LoanPeriod,
            LoanPeriodError,
            Address,
            AddressError,
            PanNumber,
            PanNumberError,
            LoanType,
            LoanTypeError,
            loading,
        } = this.state
        const { language, Signup } = this.props.app
        return (
            <SafeAreaView
                style={Styles.SafeAreaViewStyle}>
                {loading ?
                    <Loading />
                    :
                    <KeyboardAvoidingView style={Styles.ScrollViewStyle}>
                        <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
                            <View style={{ padding: Margin }}>
                                <View style={Styles.LoginInputViewStyle}>
                                    <Inputs
                                        Icons={<Icon name="user" type="antdesign" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Name[language.languagecode]}
                                        Value={Name}
                                        errorText={NameError}
                                        IsError={NameError == '' ? false : true}
                                        OnChangeText={Name => this.setState({ Name })}
                                    />
                                    <Inputs
                                        Icons={<Icon name="email" type="fontisto" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Email[language.languagecode]}
                                        Value={Email}
                                        errorText={EmailError}
                                        IsError={EmailError == '' ? false : true}
                                        OnChangeText={Email => this.setState({ Email })}
                                    />
                                    <Inputs
                                        Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.MobileNo[language.languagecode]}
                                        KeyboardType='number-pad'
                                        MaxLength={10}
                                        Value={Mobileno}
                                        errorText={MobileError}
                                        IsError={MobileError == '' ? false : true}
                                        OnChangeText={Mobileno => this.setState({ Mobileno })}
                                    />
                                    <Inputs
                                        Icons={<Icon name="location-outline" type="ionicon" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Address[language.languagecode]}
                                        Value={Address}
                                        errorText={AddressError}
                                        IsError={AddressError == '' ? false : true}
                                        OnChangeText={Address => this.setState({ Address })}
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <Inputs
                                            Icons={<Icon name="city-variant-outline" type='material-community' size={height / 40} color={Colors.Black} />}
                                            Label={TextJson.City[language.languagecode]}
                                            Value={City}
                                            errorText={CityError}
                                            Width={((width - (Margin * 8) - heightofIP)) / 2}
                                            IsError={CityError == '' ? false : true}
                                            OnChangeText={City => this.setState({ City })}
                                        />
                                        <Inputs
                                            Icons={<Icon name="city-variant-outline" type='material-community' size={height / 40} color={Colors.Black} />}
                                            Label={TextJson.State[language.languagecode]}
                                            Value={State}
                                            errorText={StateError}
                                            Width={((width - (Margin * 8) - heightofIP)) / 2}
                                            IsError={StateError == '' ? false : true}
                                            OnChangeText={State => this.setState({ State })}
                                        />
                                    </View>
                                    <Inputs
                                        Icons={<Icon name="edit" type="entypo" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Pincode[language.languagecode]}
                                        KeyboardType='number-pad'
                                        MaxLength={6}
                                        Value={Pincode}
                                        errorText={PincodeError}
                                        IsError={PincodeError == '' ? false : true}
                                        OnChangeText={Pincode => this.CheckButtonStatus('Pincode', Pincode)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="creditcard" type="antdesign" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Pancard[language.languagecode]}
                                        Value={PanNumber}
                                        errorText={PanNumberError}
                                        IsError={PanNumberError == '' ? false : true}
                                        OnChangeText={PanNumber => this.setState({ PanNumber })}
                                    />
                                    <Inputs
                                        Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.LoanAmount[language.languagecode]}
                                        KeyboardType='number-pad'
                                        Value={LoanAmount}
                                        errorText={LoanAmountError}
                                        IsError={LoanAmountError == '' ? false : true}
                                        OnChangeText={LoanAmount => this.CheckButtonStatus('LoanAmount', LoanAmount)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.LoanPeriod[language.languagecode]}
                                        KeyboardType='number-pad'
                                        Value={LoanPeriod}
                                        errorText={LoanPeriodError}
                                        IsError={LoanPeriodError == '' ? false : true}
                                        OnChangeText={LoanPeriod => this.CheckButtonStatus('LoanPeriod', LoanPeriod)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.LoanType[language.languagecode]}
                                        Value={LoanType}
                                        DisableEditable={true}
                                        errorText={LoanTypeError}
                                        IsError={LoanTypeError == '' ? false : true}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                        <View style={{ marginHorizontal: Margin }}>
                            <Buttons
                                Label={TextJson.ApplyNow[language.languagecode]}
                                OnPress={() => this.Submit(Mobileno)}
                                MarginBottom={Margin}
                            />
                        </View>
                    </KeyboardAvoidingView>
                }
            </SafeAreaView>
        )
    }
}

export default connect(loginprops, IncrDecr)(ApplyNow);