import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Dimensions,
  ScrollView,
  UIManager,
  LayoutAnimation,
  Linking,
} from 'react-native';
import { Image as Img } from 'react-native-elements';
import { connect } from 'react-redux';
import { TouchableNativeFeedback, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { IncrDecr, loginprops } from '../../redux/dispatcher/index';
import Colors from '../../Utils/Colors';
import { Styles, TitleFontSize, Margin, BorderRadius } from '../../Utils/Styles';
import NoDataFound from '../../Component/Globals/NoDataFound';
import { GetApi } from '../../Services/Api';
import Config from '../../Utils/Config';
import Loading from '../../Component/Globals/Loading';
import Carousel from 'react-native-snap-carousel';
import Image from 'react-native-scalable-image'
import ArrayJson from '../../Utils/ArrayJson';
import { TouchableHighlight } from 'react-native';
const { width, height } = Dimensions.get('window')

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ListData: [],
      isOnline: true,
      carouselHeight: 0,
    }
  }

  async componentDidMount() {
    const { login } = this.props.app
    UIManager.setLayoutAnimationEnabledExperimental(true);
    this.props.homeContentStart()
    await GetApi(Config.AllProduct, this.props)
      .then((response) => {
        console.log("Res Home", response)
        if (response && response.status == 'ok') {
          this.props.homeContentStop(response.result)
        }
        else {
          this.props.homeContentStop([])
        }
      })
  }

  CheckLogin(item) {
    const { login } = this.props.app
    if (login.loginStatus) {
      this.props.navigation.navigate('Details', { item: item })
    }
  }

  renderItem2 = ({ item, index }) => {
    return (
      <View style={{ alignItems: 'center', margin: Margin, elevation: 5, overflow: 'hidden', borderRadius: BorderRadius }}>
        <Image
          source={item.image}
          width={width - (Margin * 2)}
          onLayout={(e) => { if (index == 0) { this.setState({ carouselHeight: e.nativeEvent.layout.height }); LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut) } }}
        />
      </View>
    );
  }

  GoToUrl(type) {
    if (type == 'Loan Calculator') {
      Linking.openURL('https://cfseasymoney.com/loan-calculator.html')
    }
    else if (type == 'Loan Eligibility') {
      Linking.openURL('https://cfseasymoney.com/check-loan-eligibility.html')
    }
    else if (type == 'Car Loan') {
      Linking.openURL('https://www.cfseasymoney.com/car_loan.html')
    }
    else if (type == 'Credit Card') {
      Linking.openURL('https://www.cfseasymoney.com/credit_card.html')
    }
    else {
    }
  }

  render() {
    const { carouselHeight } = this.state
    const { login } = this.props.app
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        <View style={{ flex: 1 }}>
          <ScrollView>
            <View style={{ height: carouselHeight + (Margin * 2) }}>
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={ArrayJson.DashboardItems.Banners}
                renderItem={this.renderItem2}
                sliderWidth={width}
                itemWidth={width}
              />
            </View>
            {login.loginStatus ? null : <View style={{ alignItems: 'center', margin: Margin, marginTop: 0, elevation: 5, overflow: 'hidden', borderRadius: BorderRadius }}>
              <Image
                placeholderStyle={{ backgroundColor: Colors.Trans }}
                source={require('../../Component/assets/images/Home-page-carousel/05.png')}
                width={width - (Margin * 2)}
              />
              <View style={{ backgroundColor: 'transparent', position: 'absolute', height: '100%', width: '40%', right: 0 }} onTouchEnd={() => this.props.navigation.navigate('UserIdentification')}></View>
            </View>}
            <View style={{ alignItems: 'center', margin: Margin, marginTop: 0, elevation: 5, overflow: 'hidden', borderRadius: BorderRadius }}>
              <Image
                placeholderStyle={{ backgroundColor: Colors.Trans }}
                source={require('../../Component/assets/images/Home-page-carousel/06.png')}
                width={width - (Margin * 2)}
              />
              <View style={{ backgroundColor: 'transparent', position: 'absolute', height: '100%', width: '30%', right: 0 }} onTouchEnd={() => this.props.navigation.navigate('OurProduct')}></View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
              <TouchableWithoutFeedback
                style={{ alignItems: 'center', elevation: 5, marginBottom: Margin, overflow: 'hidden', borderRadius: BorderRadius }}
                onPress={() => this.GoToUrl('Loan Eligibility')}>
                <Image
                  placeholderStyle={{ backgroundColor: Colors.Trans }}
                  source={require('../../Component/assets/images/Home-page-carousel/07.png')}
                  width={(width - (Margin * 3)) / 2}
                />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                style={{ alignItems: 'center', elevation: 5, marginBottom: Margin, overflow: 'hidden', borderRadius: BorderRadius }}
                onPress={() => this.GoToUrl('Loan Calculator')}>
                <Image
                  placeholderStyle={{ backgroundColor: Colors.Trans }}
                  source={require('../../Component/assets/images/Home-page-carousel/08.png')}
                  width={(width - (Margin * 3)) / 2}
                />
              </TouchableWithoutFeedback>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
              <TouchableWithoutFeedback
                onPress={() => this.props.navigation.navigate('CarLoanView')}
                style={{ alignItems: 'center', elevation: 5, marginBottom: Margin, overflow: 'hidden', borderRadius: BorderRadius }}>
                <Image
                  placeholderStyle={{ backgroundColor: Colors.Trans }}
                  source={require('../../Component/assets/images/Home-page-carousel/09.png')}
                  width={(width - (Margin * 3)) / 2}
                />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => this.props.navigation.navigate('CreditCardView')}
                style={{ alignItems: 'center', elevation: 5, marginBottom: Margin, overflow: 'hidden', borderRadius: BorderRadius }}>
                <Image
                  placeholderStyle={{ backgroundColor: Colors.Trans }}
                  source={require('../../Component/assets/images/Home-page-carousel/10.png')}
                  width={(width - (Margin * 3)) / 2}
                />
              </TouchableWithoutFeedback>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(Home);