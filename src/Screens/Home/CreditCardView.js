import React from 'react';
import { View, Text, Dimensions, ScrollView } from 'react-native';
import HTML from 'react-native-render-html';

const { width, height } = Dimensions.get('window')

class CreditCardView extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
      }
   }

   render() {
      return (
         <View style={{ paddingHorizontal: width / 35 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
               <HTML source={{ html: CreditCardLoan }} />
            </ScrollView>
         </View>
      )
   }
}

export default CreditCardView;

let CreditCardLoan = `<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Credit Card | Cfs Network, Credit Card Provider, Credit Card, Credit Card Provider in Delhi, Credit Card Provider in Noida, Credit Card Provider in India, Credit Card, loan for business, Credit Card kaise le, Credit Card for new business, Credit Card apply, Credit Card against property..</title>

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Need a Credit Card? Ensure eligibility check in just 60 seconds* & apply for Credit Card online. ICICI Bank offers loan for business with overdraft facility. Apply now!..">
      <meta name="keywords" content=" Credit Card | Cfs Network, Credit Card Provider, Credit Card, Credit Card Provider in Delhi, Credit Card Provider in Noida, Credit Card Provider in India, Credit Card, loan for business, Credit Card kaise le, Credit Card for new business, Credit Card apply, Credit Card against property...">
      
      
   </head>
   <body>
      
      <div class="page-header-business">
         <div class="container">
            <img style="margin-top:10px" src="http://loan.cfseasymoney.com/public/images/Products-Banner.png01-29-2021_10:22:07" alt="Credit Card">
         </div>
      </div>
      <div class="business-loan">
         <h2>What is meant by Credit Card? (Credit Card Definition)</h2>

         <p>A credit card is a thin rectangular plastic card issued by financial institutions, which lets you borrow funds from a pre-approved limit to pay for your purchases. The limit is decided by the institution issuing the card based on your credit score and history. Generally, higher the score and better the history, higher is the limit. The key difference between a credit card and debit card is that when you swipe a debit card, the money gets deducted from your bank account; whereas, in case of a credit card, the money is taken from your pre-approved limit.</p>

         <p>Users can swipe the credit card to make a payment or use it for online transactions. After you apply for a credit card, simply make sure that the borrowed amount is repaid within the stipulated time frame to avoid penalty charges. Your credit card details are always secured with the card issuer and you should not share your credit card information with anyone to avoid fraud.</p>

         <p>Bajaj Finserv offers different types of credit cards to suit your diverse needs. Moreover, the Bajaj Finserv RBL Bank Credit Card has great offers and benefits to make shopping a rewarding experience for you.</p>

         <h3>How to use a credit card?</h3>

         <p>A credit card is the ideal financial tool if you know how to use it to your benefit. You can avail advances through your credit card and enjoy an interest-free period of up to 50 days. You need not pay additional interests once you repay the utilised amount within this grace period.</p>

         <h3>How do Credit Cards Work?</h3>

         <p>When you use a credit card for any transaction, the merchant fee, on your behalf, is paid by the financial institution issuing you the card. You can make as many transactions as you want within your approved credit limit.</p>

         <p>You get an account statement each month summarizing the transaction details of your card.</p>

         <p>The statement also highlights the following:</p>

         <ul>
            <li>Available credit and cash limit</li>
            <li>Minimum payment due</li>
            <li>Total payment due date</li>
            <li>Interest and fees charged</li>
            <li>Payment methods</li>
         </ul>

         <h3>What is the difference between a credit card and a debit card?</h3>

         <p>Debit cards and credit cards offer you the convenience of making online and offline payments, thus allowing you to travel or shop without having to carry cash in your wallet. While they are both physical cards that allow you to make cashless transactions, there are several key differences between credit cards and debit cards. Debit cards allow bank customers to spend money by drawing on funds they have deposited at the bank. Credit cards allow consumers to borrow money from the card issuer up to a certain limit in order to purchase items or withdraw cash.</p>

      </div>
   </body>
</html>`