import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Dimensions,
  FlatList,
  UIManager,
  LayoutAnimation,
  Alert,
} from 'react-native';
import { Image as Img } from 'react-native-elements';
import { connect } from 'react-redux';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import Colors from '../../../Utils/Colors';
import { Styles, TitleFontSize, Margin, BorderRadius } from '../../../Utils/Styles';
import NoDataFound from '../../../Component/Globals/NoDataFound';
import { GetApi } from '../../../Services/Api';
import Config from '../../../Utils/Config';
import Loading from '../../../Component/Globals/Loading';
import Carousel from 'react-native-snap-carousel';
import Image from 'react-native-scalable-image'
const { width, height } = Dimensions.get('window')

class OurProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ListData: [],
      isOnline: true,
      carouselHeight: 0,
    }
  }

  async componentDidMount() {
    const { login } = this.props.app
    UIManager.setLayoutAnimationEnabledExperimental(true);
    this.props.homeContentStart()
    await GetApi(Config.AllProduct, this.props)
      .then((response) => {
        console.log("Res Home", response)
        if (response && response.status == 'ok') {
          this.props.homeContentStop(response.result)
        }
        else {
          this.props.homeContentStop([])
        }
      })
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={Styles.DashboardView}>
        <TouchableNativeFeedback style={Styles.DashboardTabMainView} onPress={() => this.props.navigation.navigate('Details', { item: item })}>
          <Img
            // source={require('../../Component/assets/images/Products/Business-Loan.png')}
            source={{ uri: Config.ImageUrl + item.product_icon }}
            placeholderStyle={{ backgroundColor: Colors.Trans }}
            style={{ width: width / 18, height: width / 18 }}
          />
          <View style={Styles.DashboardTitleView}>
            <Text style={Styles.OurProductTextStyle}>{item.product_name}</Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    )
  }

  renderItem2 = ({ item, index }) => {
    return (
      <View style={{ alignItems: 'center', margin: Margin, elevation: 5, overflow: 'hidden', borderRadius: BorderRadius, backgroundColor: Colors.White }} onTouchEnd={() => this.props.navigation.navigate('Details', { item: item })}>
        <Image
          source={{ uri: Config.ImageUrl + item.product_image }}
          width={width - (Margin * 2)}
          onLayout={(e) => { if (index == 0) { this.setState({ carouselHeight: e.nativeEvent.layout.height }); LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut) } }}
        />
      </View>
    );
  }

  render() {
    const { carouselHeight } = this.state
    const { language, homeContent } = this.props.app
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {homeContent.loading ?
          <Loading />
          :
          <View style={Styles.DashboardMainView}>
            <View style={{ height: carouselHeight + (Margin * 2) }}>
              <Carousel
                ref={(c) => { this._carousel = c; }}
                data={homeContent.data}
                renderItem={this.renderItem2}
                sliderWidth={width}
                itemWidth={width}
              />
            </View>
            {homeContent && homeContent.data && homeContent.data.length > 0 ?
              <FlatList
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={{ width: '100%' }}
                data={homeContent.data}
                extraData={this.state}
                keyExtractor={(item, index) => String(index)}
                renderItem={this.renderItem}
              />
              : <NoDataFound
                language={language}
              />
            }
          </View>
        }
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(OurProduct);