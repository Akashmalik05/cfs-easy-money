import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Dimensions,
  FlatList,
  Linking,
} from 'react-native';
import { Icon, Image } from 'react-native-elements';
import { connect } from 'react-redux';
import { TouchableNativeFeedback, ScrollView } from 'react-native-gesture-handler';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import Colors from '../../../Utils/Colors';
import { Styles, Margin, TitleFontSize, ValueFontSize, BigTitleFontSize, BorderRadius } from '../../../Utils/Styles';
import NoDataFound from '../../../Component/Globals/NoDataFound';
import Buttons from '../../../Component/Globals/Buttons';
import TextJson from '../../../Utils/TextJson';
import { SetOnlineOffline } from '../../../Utils/ValidationFunctions';
import { GetApi } from '../../../Services/Api';
import Config from '../../../Utils/Config';
import Loading from '../../../Component/Globals/Loading';
// import Image from 'react-native-scalable-image';

const { width, height } = Dimensions.get('window')

class AboutUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ListData: [],
      isOnline: true,
    }
  }

  async componentDidMount() {
    const { login } = this.props.app
    await GetApi(Config.HomeContent, this.props)
      .then((response) => {
        console.log("Resss Home", response)
        this.setState({ ListData: response, loading: false })
      })
    // this.setState({ ListData: ArrayJson.DashboardItems[login.usertype] })
  }

  GoToUrl(type) {
    if (type == 'T&C') {
      Linking.openURL('https://cfseasymoney.com/terms_and_conditions.html')
    }
    else if (type == 'PP') {
      Linking.openURL('https://cfseasymoney.com/privacy_policy.html')
    }
    else {
    }
  }

  render() {
    console.log(this.props)
    const { loading, ListData } = this.state
    const { language } = this.props.app
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {loading ?
          <Loading />
          :
          <>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{ alignItems: 'center', marginTop: Margin, marginHorizontal: Margin, borderRadius: BorderRadius, elevation: 5, overflow: 'hidden' }}>
                <Image
                  placeholderStyle={{ backgroundColor: Colors.Trans }}
                  source={require('../../../Component/assets/images/aboutus.jpg')}
                  style={{ width: width - (Margin * 2), height: height / 5 }}
                  resizeMode='cover'
                />
              </View>
              <View style={{ margin: Margin }}>
                <Text style={{ color: Colors.TextBlack, fontWeight: 'bold', fontSize: BigTitleFontSize }}>{TextJson.AboutUsTitle[language.languagecode]}</Text>
                <Text style={{ color: Colors.TextGrey, marginTop: Margin / 2, fontSize: ValueFontSize, lineHeight: Margin * 2, textAlign: 'justify' }}>{TextJson.AboutUsContent[language.languagecode]}</Text>
              </View>
            </ScrollView>
            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginVertical: Margin }}>
              <Text style={{ color: Colors.Blue, fontWeight: 'bold', fontSize: TitleFontSize }} onPress={() => this.GoToUrl('T&C')}>{TextJson.TermsConsitions[language.languagecode]} / </Text>
              <Text style={{ color: Colors.Blue, fontWeight: 'bold', fontSize: TitleFontSize }} onPress={() => this.GoToUrl('PP')}>{TextJson.PrivacyPolicy[language.languagecode]}</Text>
            </View>
          </>
        }
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(AboutUs);