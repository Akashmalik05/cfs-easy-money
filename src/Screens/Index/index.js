import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../redux/dispatcher/index';
import { StackActions } from '@react-navigation/native';
import { Image } from 'react-native-elements';
import Colors from '../../Utils/Colors';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    // if (this.props.app.login.loginStatus) {
    //   this.props.navigation.dispatch(
    //     StackActions.replace('DrawerApp')
    //   );
    // }
    // else {
    //   this.props.navigation.dispatch(
    //     StackActions.replace('UserIdentification')
    //   );
    // }
    this.props.navigation.dispatch(
      StackActions.replace('DrawerApp')
    );

  }

  render() {
    return (
      <View style={{ flex: 1, width: '100%' }}>
        <Image
          placeholderStyle={{ backgroundColor: Colors.Trans }}
          source={require('../../Component/assets/images/Splash/Png/splash1024x512.png')}
          style={{ width: '100%', height: '100%' }}
          resizeMode='contain'
        />
      </View>
    )
  }
}

export default connect(loginprops, IncrDecr)(Index);