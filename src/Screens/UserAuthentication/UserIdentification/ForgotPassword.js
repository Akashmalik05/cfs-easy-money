import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  Dimensions,
  TextInput,
  Animated,
  Platform,
  UIManager,
  LayoutAnimation,
  Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Styles, Margin } from '../../../Utils/Styles';
import Inputs from '../../../Component/Globals/Inputs';
import RadioButtons from '../../../Component/Globals/RadioButtons';
import ArrayJson from '../../../Utils/ArrayJson';
import TextJson from '../../../Utils/TextJson';
import Buttons from '../../../Component/Globals/Buttons';
import * as Function from '../../../Utils/ValidationFunctions';
import Colors from '../../../Utils/Colors';
import LoginNote from '../../../Component/Globals/LoginNote';
import Logo from '../../../Component/Globals/Logo';
import { PostApi, GetApi } from '../../../Services/Api';
import Config from '../../../Utils/Config';
import ActivityIndicator from 'react-native-paper/lib/module/components/ActivityIndicator';
import Loading from '../../../Component/Globals/Loading'
import { StackActions } from '@react-navigation/native';

const { width, height } = Dimensions.get('window')

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      Mobileno: '',
      Password: '',
      MobileError: '',
      PasswordError: '',
      ConfirmPassword: '',
      ConfirmPasswordError: '',
    }
  }

  async componentDidMount() {
    console.log("this.props.route.params", this.props.route.params);
    let languagedata = await Function.checkDefaultLanguage(ArrayJson.ChooseLanguage, this.props && this.props.app && this.props.app.language && this.props.app.language.languagecode ? this.props.app.language.languagecode : 'en')
    this.setState({ LanguageArray: languagedata.data, LanguageCode: languagedata.code, loading: false })
    if (Platform.OS == 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true)
    }
  }

  async Submit() {
    const { Mobileno, Password, ConfirmPassword } = this.state;
    if (Mobileno.trim().length != 10 || isNaN(Mobileno)) {
      alert("Please enter valid mobile no.")
    }
    else if (Password != ConfirmPassword) {
      alert('Password and Confirm password are not same.')
    }
    else {
      this.setState({ loading: true })
      let response = await Function.ForgotPassword(Mobileno, Password, this.props)
      console.log(response)
      if (response) {
        if (response.status == 'error') {
          alert(response.result)
        }
        else {
          if (JSON.stringify(response).includes("Phone Number is incorrect or does exist in Database")) {
            alert("Phone Number is incorrect or does exist in Database")
          }
          else if (JSON.stringify(response).includes("Password Updated")) {
            this.props.navigation.goBack(null)
          }
        }
        this.setState({ loading: false })
      }
    }
  }

  CheckButtonStatus(type, Text) {
    if (type == 'Mobile') {
      if (this.state.Password != '' && this.state.ConfirmPassword != '' && Text != '') {
        this.setState({ Mobileno: Text, isCheckedAgreement: true })
      }
      else {
        this.setState({ Mobileno: Text, isCheckedAgreement: false })
      }
    }
    else if (type == 'Password') {
      if (this.state.Mobileno != '' && this.state.ConfirmPassword != '' && Text != '') {
        this.setState({ Password: Text, isCheckedAgreement: true })
      }
      else {
        this.setState({ Password: Text, isCheckedAgreement: false })
      }
    }
    else {
      if (this.state.Mobileno != '' && this.state.Password != '' && Text != '') {
        this.setState({ ConfirmPassword: Text, isCheckedAgreement: true })
      }
      else {
        this.setState({ ConfirmPassword: Text, isCheckedAgreement: false })
      }
    }
  }

  render() {
    const {
      Mobileno,
      Password,
      LanguageCode,
      isCheckedAgreement,
      MobileError,
      PasswordError,
      ConfirmPassword,
      ConfirmPasswordError,
      loading
    } = this.state
    const { login } = this.props.app
    console.log("login", login)
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {loading ?
          <Loading />
          :
          <KeyboardAvoidingView style={Styles.ScrollViewStyle}>
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
              <Logo />
              <View style={{ flex: 1, paddingHorizontal: Margin }}>
                <View style={Styles.LoginInputViewStyle}>
                  <Inputs
                    Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                    Label={TextJson.MobileNo[LanguageCode]}
                    Value={Mobileno}
                    KeyboardType={'number-pad'}
                    errorText={MobileError}
                    MaxLength={10}
                    IsError={MobileError == '' ? false : true}
                    OnChangeText={Mobileno => this.CheckButtonStatus('Mobile', Mobileno)}
                  />
                  <Inputs
                    Icons={<Icon name="lock-outline" size={height / 40} color={Colors.Black} />}
                    Label={TextJson.Password[LanguageCode]}
                    Value={Password}
                    errorText={PasswordError}
                    SecureTextEntry
                    IsError={PasswordError == '' ? false : true}
                    OnChangeText={Password => this.CheckButtonStatus('Password', Password)}
                  />
                  <Inputs
                    Icons={<Icon name="lock-outline" size={height / 40} color={Colors.Black} />}
                    Label={TextJson.ConfirmPassword[LanguageCode]}
                    Value={ConfirmPassword}
                    errorText={ConfirmPasswordError}
                    // SecureTextEntry
                    IsError={ConfirmPasswordError == '' ? false : true}
                    OnChangeText={Password => this.CheckButtonStatus('ConfirmPassword', Password)}
                  />
                </View>
              </View>
            </ScrollView>
            <View style={{ marginHorizontal: Margin }}>
              <Buttons
                Disabled={!isCheckedAgreement}
                Label={TextJson.UpdatePassword[LanguageCode]}
                OnPress={() => this.Submit(Mobileno)}
                MarginBottom={Margin}
              />
            </View>
          </KeyboardAvoidingView>}
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(ForgotPassword);