import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  KeyboardAvoidingView,
  Dimensions,
  TextInput,
  Animated,
  Platform,
  UIManager,
  LayoutAnimation,
  Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Styles, Margin } from '../../../Utils/Styles';
import Inputs from '../../../Component/Globals/Inputs';
import RadioButtons from '../../../Component/Globals/RadioButtons';
import ArrayJson from '../../../Utils/ArrayJson';
import TextJson from '../../../Utils/TextJson';
import Buttons from '../../../Component/Globals/Buttons';
import * as Function from '../../../Utils/ValidationFunctions';
import Colors from '../../../Utils/Colors';
import LoginNote from '../../../Component/Globals/LoginNote';
import Logo from '../../../Component/Globals/Logo';
import { PostApi, GetApi } from '../../../Services/Api';
import Config from '../../../Utils/Config';
import ActivityIndicator from 'react-native-paper/lib/module/components/ActivityIndicator';
import Loading from '../../../Component/Globals/Loading'
import { StackActions } from '@react-navigation/native';

const { width, height } = Dimensions.get('window')

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      Mobileno: '',
      Password: '',
      MobileError: '',
      PasswordError: '',
    }
  }

  async componentDidMount() {
    console.log("this.props.route.params", this.props.route.params);
    let languagedata = await Function.checkDefaultLanguage(ArrayJson.ChooseLanguage, this.props && this.props.app && this.props.app.language && this.props.app.language.languagecode ? this.props.app.language.languagecode : 'en')
    this.setState({ LanguageArray: languagedata.data, LanguageCode: languagedata.code, loading: false })
    if (Platform.OS == 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true)
    }
  }

  async Submit() {
    const { Mobileno, Password } = this.state;
    let response = await Function.SubmitLogin(Mobileno, Password, this.props)
    console.log(response)
    if (response) {
      if (response.status == 'error') {
        alert(response.result)
        this.props.loginstop({})
      }
      else if (response.status == 'ok') {
        this.props.loginstop(response)
        if (this.props && this.props.route && this.props.route.params && this.props.route.params.Type && this.props.route.params.Type == 'Apply') {
          this.props.navigation.dispatch(
            StackActions.replace('ApplyNow', { LoanType: this.props.route.params.LoanType })
          );
        }
        else {
          this.props.navigation.goBack(null)
        }
      }
      else {
        console.log("err", response)
        this.props.loginstop({})
      }
    }
    else {
      this.props.loginstop({})
    }
  }

  CheckButtonStatus(type, Text) {
    if (type == 'Mobile') {
      if (this.state.Password != '' && Text != '') {
        this.setState({ Mobileno: Text, isCheckedAgreement: true })
      }
      else {
        this.setState({ Mobileno: Text, isCheckedAgreement: false })
      }
    }
    else {
      if (this.state.Mobileno != '' && Text != '') {
        this.setState({ Password: Text, isCheckedAgreement: true })
      }
      else {
        this.setState({ Password: Text, isCheckedAgreement: false })
      }
    }
  }

  render() {
    const {
      Mobileno,
      Password,
      LanguageCode,
      isCheckedAgreement,
      MobileError,
      PasswordError
    } = this.state
    const { login, Signup } = this.props.app
    console.log("login", login, Signup)
    return (
      <SafeAreaView
        style={Styles.SafeAreaViewStyle}>
        {login.loading ?
          <Loading />
          :
          <KeyboardAvoidingView style={Styles.ScrollViewStyle}>
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
              <Logo />
              <View style={{ flex: 1, paddingHorizontal: Margin }}>
                <View style={Styles.LoginInputViewStyle}>
                  <Inputs
                    Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                    Label={`${TextJson.MobileNo[LanguageCode]}/${TextJson.Email[LanguageCode]}`}
                    Value={Mobileno}
                    errorText={MobileError}
                    IsError={MobileError == '' ? false : true}
                    OnChangeText={Mobileno => this.CheckButtonStatus('Mobile', Mobileno)}
                  />
                  <Inputs
                    Icons={<Icon name="lock-outline" size={height / 40} color={Colors.Black} />}
                    Label={TextJson.Password[LanguageCode]}
                    Value={Password}
                    errorText={PasswordError}
                    SecureTextEntry
                    IsError={PasswordError == '' ? false : true}
                    OnChangeText={Password => this.CheckButtonStatus('Password', Password)}
                  />
                </View>
                <View style={{ alignItems: 'center' }}>
                  <Text style={{ fontSize: height / 50, marginBottom: Margin }} onPress={() => this.props.navigation.navigate('ForgotPassword')}>{TextJson.ForgotPassword[LanguageCode]}?</Text>
                  <Text style={Styles.RegisterasText} onPress={() => this.props.navigation.navigate('Signup')}>Register as new user</Text>
                </View>
              </View>
            </ScrollView>
            <View style={{ marginHorizontal: Margin }}>
              <Buttons
                Disabled={!isCheckedAgreement}
                Label={TextJson.Continue[LanguageCode]}
                OnPress={() => this.Submit(Mobileno)}
                MarginBottom={Margin}
              />
            </View>
          </KeyboardAvoidingView>}
      </SafeAreaView>
    )
  }
}

export default connect(loginprops, IncrDecr)(Home);

















// import React from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
// } from 'react-native';
// import {connect} from 'react-redux';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import {IncrDecr, loginprops} from '../../redux/dispatcher/index';
// import { StackActions } from '@react-navigation/native';

// class Login extends React.Component{
//   constructor(props) {
//       super(props);
//       this.state = {
//       }
//   }

//   render()
//   {
//     return(
//       <View style={{alignItems:'center', justifyContent:'center', flex:1, width:'100%'}}>
//           <Text style={{fontSize:50}} onPress={()=>this.props.navigation.setParams({})}>Login Screen</Text>
//           <TouchableOpacity style={{backgroundColor:'grey', width:'100%', marginBottom:10}} onPress={()=>{this.props.login('8979867545');this.props.navigation.dispatch(
//         StackActions.replace('DrawerApp')
//       );}}>
//             <Text>login</Text>
//           </TouchableOpacity>
//       </View>
//     )
//   }
// }

// export default connect(loginprops, IncrDecr)(Login);