import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    KeyboardAvoidingView,
    Dimensions,
    TextInput,
    Animated,
    Platform,
    UIManager,
    LayoutAnimation
} from 'react-native';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../../redux/dispatcher/index';
import { Icon } from 'react-native-elements';
import { Styles, Margin } from '../../../Utils/Styles';
import Inputs from '../../../Component/Globals/Inputs';
import RadioButtons from '../../../Component/Globals/RadioButtons';
import ArrayJson from '../../../Utils/ArrayJson';
import TextJson from '../../../Utils/TextJson';
import Buttons from '../../../Component/Globals/Buttons';
import * as Function from '../../../Utils/ValidationFunctions';
import Colors from '../../../Utils/Colors';
import LoginNote from '../../../Component/Globals/LoginNote';
import Logo from '../../../Component/Globals/Logo';
import { PostApi, GetApi } from '../../../Services/Api';
import Config from '../../../Utils/Config';
import ActivityIndicator from 'react-native-paper/lib/module/components/ActivityIndicator';
import Loading from '../../../Component/Globals/Loading'
import { Input } from 'react-native-elements';

const { width, height } = Dimensions.get('window')

let MaxHeight = 0
let MaxHeight2 = 0

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Mobileno: '',
            Password: '',
            LanguageCode: 'en',
            isCheckedAgreement: '',
            MobileError: '',
            PasswordError: '',
            ConfirmPassword: '',
            ConfirmPasswordError: '',
            Email: '',
            EmailError: '',
            Name: '',
            NameError: '',
        }
    }

    async componentDidMount() {
        let languagedata = await Function.checkDefaultLanguage(ArrayJson.ChooseLanguage, this.props.app.language.languagecode)
        this.setState({ LanguageArray: languagedata.data, LanguageCode: languagedata.code })
        if (Platform.OS == 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    async Submit(MobileNo) {
        const {
            Mobileno,
            Password,
            Email,
            Name,
            ConfirmPassword
        } = this.state

        let regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regexMobile = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (Password != ConfirmPassword) {
            this.setState({ ConfirmPasswordError: 'Password and Confirm Password are not same.' })
        }
        else if (!regexEmail.test(Email)) {
            this.setState({ EmailError: 'Please enter valid email.' })
        }
        else if (!regexMobile.test(MobileNo)) {
            this.setState({ MobileError: 'Please enter valid Mobile number.' })
        }
        else {
            let body = {
                "name": Name,
                "password": Password,
                "email": Email,
                "phone_no": MobileNo
            }
            let response = await Function.SubmitSignup(body, this.props)
            console.log("hjhjjj", response)
            if (response) {
                if (response.status == 'error') {
                    alert(response.result)
                    this.props.signupstop({})
                }
                else if (response.status == 'ok') {
                    alert("Account created successfully.")
                    this.props.signupstop(response)
                    this.props.navigation.navigate("UserIdentification")
                }
                else {
                    console.log("err", response)
                    this.props.signupstop({})
                }
            }
            else {
                this.props.signupstop({})
            }
        }
    }

    CheckButtonStatus(type, Text) {
        const {
            Mobileno,
            Password,
            LanguageCode,
            isCheckedAgreement,
            MobileError,
            PasswordError,
            ConfirmPassword,
            ConfirmPasswordError,
            Email,
            EmailError,
            Name,
            NameError,
        } = this.state

        if (type == 'Name') {
            if (Password != '' && ConfirmPassword != '' && Email != '' && Mobileno != '' && Text != '' && Mobileno.length == 10) {
                this.setState({ Name: Text, isCheckedAgreement: true })
            }
            else {
                this.setState({ Name: Text, isCheckedAgreement: false })
            }
        }
        else if (type == 'Email') {
            if (Password != '' && ConfirmPassword != '' && Mobileno != '' && Name != '' && Text != '' && Mobileno.length == 10) {
                this.setState({ Email: Text, isCheckedAgreement: true })
            }
            else {
                this.setState({ Email: Text, isCheckedAgreement: false })
            }
        }
        else if (type == 'Mobile') {
            if (Password != '' && ConfirmPassword != '' && Email != '' && Name != '' && Text != '' && Text.length == 10) {
                this.setState({ Mobileno: Text, isCheckedAgreement: true })
            }
            else {
                this.setState({ Mobileno: Text, isCheckedAgreement: false })
            }
        }
        else if (type == 'Password') {
            if (ConfirmPassword != '' && Mobileno != '' && Email != '' && Name != '' && Text != '' && Mobileno.length == 10) {
                this.setState({ Password: Text, isCheckedAgreement: true })
            }
            else {
                this.setState({ Password: Text, isCheckedAgreement: false })
            }
        }
        else {
            if (Password != '' && Mobileno != '' && Email != '' && Name != '' && Text != '' && Mobileno.length == 10) {
                this.setState({ ConfirmPassword: Text, isCheckedAgreement: true })
            }
            else {
                this.setState({ ConfirmPassword: Text, isCheckedAgreement: false })
            }
        }
    }

    render() {
        const {
            loading,
            Mobileno,
            Password,
            LanguageCode,
            isCheckedAgreement,
            MobileError,
            PasswordError,
            ConfirmPassword,
            ConfirmPasswordError,
            Email,
            EmailError,
            Name,
            NameError,
        } = this.state
        const { language, Signup } = this.props.app
        return (
            <SafeAreaView
                style={Styles.SafeAreaViewStyle}>
                {Signup.loading ?
                    <Loading />
                    :
                    <KeyboardAvoidingView style={Styles.ScrollViewStyle}>
                        <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled'>
                            <Logo />
                            <View style={{ paddingHorizontal: Margin }}>
                                <View style={Styles.LoginInputViewStyle}>
                                    <Inputs
                                        Icons={<Icon name="user" type="antdesign" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Name[LanguageCode]}
                                        Value={Name}
                                        errorText={NameError}
                                        IsError={NameError == '' ? false : true}
                                        OnChangeText={Name => this.CheckButtonStatus('Name', Name)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="email" type="fontisto" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Email[LanguageCode]}
                                        Value={Email}
                                        errorText={EmailError}
                                        IsError={EmailError == '' ? false : true}
                                        OnChangeText={Email => this.CheckButtonStatus('Email', Email)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="phone-android" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.MobileNo[LanguageCode]}
                                        KeyboardType='number-pad'
                                        MaxLength={10}
                                        Value={Mobileno}
                                        errorText={MobileError}
                                        IsError={MobileError == '' ? false : true}
                                        OnChangeText={Mobileno => this.CheckButtonStatus('Mobile', Mobileno)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="lock-outline" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.Password[LanguageCode]}
                                        Value={Password}
                                        SecureTextEntry
                                        errorText={PasswordError}
                                        IsError={PasswordError == '' ? false : true}
                                        OnChangeText={Password => this.CheckButtonStatus('Password', Password)}
                                    />
                                    <Inputs
                                        Icons={<Icon name="lock-outline" size={height / 40} color={Colors.Black} />}
                                        Label={TextJson.ConfirmPassword[LanguageCode]}
                                        Value={ConfirmPassword}
                                        SecureTextEntry
                                        errorText={ConfirmPasswordError}
                                        IsError={ConfirmPasswordError == '' ? false : true}
                                        OnChangeText={ConfirmPassword => this.CheckButtonStatus('ConfirmPassword', ConfirmPassword)}
                                    />
                                </View>
                            </View>
                        </ScrollView>
                        <View style={{ marginHorizontal: Margin }}>
                            <Buttons
                                Disabled={!isCheckedAgreement}
                                Label={TextJson.Signup[LanguageCode]}
                                OnPress={() => this.Submit(Mobileno)}
                                MarginBottom={Margin}
                            />
                        </View>
                    </KeyboardAvoidingView>}
            </SafeAreaView>
        )
    }
}

export default connect(loginprops, IncrDecr)(Signup);