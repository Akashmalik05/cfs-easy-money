import React from 'react';
import { Text, Dimensions, View } from 'react-native';
import Colors from '../../Utils/Colors';
import RadioButton from 'react-native-paper/lib/module/components/RadioButton/RadioButton';
import Checkbox from 'react-native-paper/lib/module/components/Checkbox/Checkbox';
import { Styles } from '../../Utils/Styles';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';

const { width, height } = Dimensions.get('window')

class RadioButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { Label, Condition, OnPress, Type } = this.props;
    return (
      <View style={Styles.RadioTNF}>
        {Type == 'Radio' ? <RadioButton
          color={Colors.Blue}
          style={{ marginLeft: 0, paddingLeft: 0 }}
          status={Condition ? 'checked' : 'unchecked'}
          onPress={OnPress}
        />
          :
          <Checkbox
            color={Colors.Blue}
            style={{ marginLeft: 0, paddingLeft: 0 }}
            status={Condition ? 'checked' : 'unchecked'}
            onPress={OnPress}
          />
        }
        <Text style={Styles.RadioLabelStyle}>{Label}</Text>
      </View>
    )
  }
}

export default RadioButtons;