import React from 'react';
import Button from 'react-native-paper/lib/module/components/Button';
import { Styles, heightofIP, ValueFontSize } from '../../Utils/Styles';
import { View } from 'react-native';
import Colors from '../../Utils/Colors';

class Buttons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { Label, OnPress, Disabled, Width, MarginBottom, MarginVertical } = this.props;
    return (
      <Button
        mode='contained'
        disabled={Disabled}
        color={Colors.Blue}
        style={[Styles.ButtonStyle, { width: Width ? Width : '100%', marginBottom: MarginBottom ? MarginBottom : 0 }]}
        labelStyle={[Styles.ButtonLabel, { marginVertical: MarginVertical ? MarginVertical : (heightofIP - ValueFontSize) / 2, }]}
        onPress={OnPress}
      >{Label}</Button>
    )
  }
}

export default Buttons;