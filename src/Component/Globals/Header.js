import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Styles } from '../../Utils/Styles';
import { Icon, Badge } from 'react-native-elements';
import Colors from '../../Utils/Colors';

const { width, height } = Dimensions.get('window')

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { Label, OnPress, LeftIconHide, Title, BadgeValue, OnMenuPress, Type, RightIcon } = this.props;
        return (
            <View style={Styles.HeaderMainView}>
                <View>
                    <Icon
                        name={Type == 'Back' ? 'chevron-left' : 'menu'}
                        type='entypo'
                        size={height / 30}
                        color={LeftIconHide ? Colors.Blue : Colors.White}
                        onPress={LeftIconHide ? () => null : OnMenuPress}
                    />
                </View>
                <View style={Styles.HeaderTitleView}>
                    <Text style={Styles.HeaderTitleStyle} numberOfLines={1}>{Title}</Text>
                </View>
                <View style={{ width: height / 30, alignItems: 'flex-end' }}>
                </View>
            </View>
        )
    }
}

export default Header;