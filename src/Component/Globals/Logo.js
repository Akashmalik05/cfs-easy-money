import React from 'react';
import { Text, Dimensions, View } from 'react-native';
import { Image } from 'react-native-elements';
import { Styles, Margin } from '../../Utils/Styles';
import Colors from '../../Utils/Colors';
// import Image from 'react-native-scalable-image';

const { width, height } = Dimensions.get('window')

class Logo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { Content } = this.props;
    return (
      <View style={Styles.LogoViewStyle}>
        <View style={Styles.LogoViewStyle2}>
          <Image
            source={require('../assets/images/Logo/logo512x512.png')}
            width={width / 2}
            placeholderStyle={{ backgroundColor: Colors.Trans }}
            style={Styles.LoginLogoStyle}
          />
        </View>
      </View>
    )
  }
}

export default Logo;