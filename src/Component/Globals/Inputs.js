import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import TextInput from 'react-native-paper/lib/module/components/TextInput/TextInput';
import Colors from '../../Utils/Colors';
import { Styles, heightofIP, Margin } from '../../Utils/Styles';

const { width, height } = Dimensions.get('window')

class Inputs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { Icons, Label, Width, DisableEditable, Value, OnChangeText, KeyboardType, SecureTextEntry, MaxLength, errorText, IsError } = this.props;
    return (
      <View style={{ alignSelf: 'baseline' }}>
        <View style={Styles.InputMainView}>
          <View style={Styles.InputIconView}>
            <View style={Styles.InputIconChildView}>
              {Icons}
            </View>
          </View>
          <View style={Styles.InputViewStyle}>
            <TextInput
              label={Label}
              value={Value}
              secureTextEntry={SecureTextEntry}
              maxLength={MaxLength}
              keyboardType={KeyboardType}
              style={[Styles.InputStyle, { width: Width ? Width : (width - (Margin * 2) - heightofIP) }]}
              theme={{ colors: { primary: Colors.LightBlue }, roundness: 2 }}
              mode='outlined'
              onChangeText={OnChangeText}
              editable={DisableEditable ? false : true}
            />
          </View>
        </View>
        <Text style={{ color: IsError ? Colors.Red : Colors.Trans }}>{errorText}</Text>
      </View>
    )
  }
}

export default Inputs;