import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Styles } from '../../Utils/Styles';
import { Icon, Badge } from 'react-native-elements';
import Colors from '../../Utils/Colors';
import { ActivityIndicator } from 'react-native-paper';

const { width, height } = Dimensions.get('window')

class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <View style={Styles.Loading}>
                <ActivityIndicator color={Colors.Blue} size={height / 30} />
            </View>
        )
    }
}

export default Loading;