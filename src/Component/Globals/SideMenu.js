import React from 'react';
import { View, Text, Dimensions, FlatList } from 'react-native';
import { Styles, Margin, ValueFontSize } from '../../Utils/Styles';
import { Icon, Badge, Image } from 'react-native-elements';
import Colors from '../../Utils/Colors';
import TextJson from '../../Utils/TextJson';
import { connect } from 'react-redux';
import { IncrDecr, loginprops } from '../../redux/dispatcher/index';
import { TouchableNativeFeedback, ScrollView } from 'react-native-gesture-handler';
import { StackActions } from '@react-navigation/native';
import { version } from '../../../package.json'
import ArrayJson from '../../Utils/ArrayJson';
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('window')

class SideMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ScreenName: ''
        }
    }

    renderItem = ({ item, index }) => {
        const { language, login } = this.props.app;
        const { ScreenName } = this.state;
        let Color
        if (item.title[language.languagecode] == 'About Us') {
            if (ScreenName == 'AboutUs') {
                Color = Colors.TextGrey
            }
            else {
                Color = Colors.TextGrey
            }
        }
        else {
            if (ScreenName == 'OurProduct') {
                Color = Colors.TextGrey
            }
            else {
                Color = Colors.TextGrey
            }
        }
        if ((item.title[language.languagecode] == 'Login' || item.title[language.languagecode] == 'Logout')) {
            if (login.loginStatus === false && item.title[language.languagecode] == 'Login') {
                return (
                    <TouchableNativeFeedback style={Styles.SideMenuTabs} onPress={() => {
                        this.props.navigation.navigate(item.screen)
                    }}>
                        <Image
                            source={item.Image}
                            placeholderStyle={{ backgroundColor: Colors.White }}
                            style={{ width: width / 22, height: width / 22, marginRight: Margin }}
                        />
                        <Text style={[Styles.SideMenuTabsTextStyle, { color: Color }]}>{item.title[language.languagecode]}</Text>
                    </TouchableNativeFeedback>
                )
            }
            else if (login.loginStatus === true && item.title[language.languagecode] == 'Logout') {
                return (
                    <TouchableNativeFeedback style={Styles.SideMenuTabs} onPress={() => {
                        this.props.logout(); this.props.navigation.navigate('Home')
                    }}>
                        <Image
                            source={item.Image}
                            style={{ width: width / 22, height: width / 22, marginRight: Margin }}
                        />
                        <Text style={[Styles.SideMenuTabsTextStyle, { color: Color }]}>{item.title[language.languagecode]}</Text>
                    </TouchableNativeFeedback>
                )
            }
            else {
                return (null)
            }
        }
        else {
            return (
                <TouchableNativeFeedback style={Styles.SideMenuTabs} onPress={() => {
                    this.props.navigation.navigate(item.screen)
                }}>
                    <Image
                        source={item.Image}
                        style={{ width: width / 22, height: width / 22, marginRight: Margin }}
                    />
                    <Text style={[Styles.SideMenuTabsTextStyle, { color: Color }]}>{item.title[language.languagecode]}</Text>
                </TouchableNativeFeedback>
            )
        }
    }

    CheckScreen(State = null) {
        if (State) {
            if (State.routes && State.routes.length > 0) {
                let routes = State.routes
                if (routes[State.index] && routes[State.index].state) {
                    this.CheckScreen(routes[State.index].state)
                }
                else {
                    if (this.state.ScreenName != routes[State.index].name) { this.setState({ ScreenName: routes[State.index].name }) }
                }
            }
        }
    }

    render() {
        const { language, login } = this.props.app;
        console.log("rrrr", this.props)
        this.CheckScreen(this.props.state)
        return (
            <View style={Styles.SideMenuMainView}>
                <View style={{ flex: 1 }}>
                    {/* <View style={{ backgroundColor: Colors.MidBlue, alignItems: 'center' }}> */}
                    <View style={{ backgroundColor: Colors.White, borderBottomWidth: 2, borderBottomColor: Colors.Blue, alignItems: 'flex-start', paddingVertical: Margin * 2 }}>
                        <View style={Styles.SideMenuImage}>
                            <Image source={require('../assets/images/Logo/logo512x512.png')}
                                placeholderStyle={{ backgroundColor: Colors.Trans }}
                                resizeMethod='resize'
                                resizeMode='cover'
                                style={Styles.SideMenuImg} />
                        </View>
                        {login.loginStatus ? <Text style={[Styles.UserNameStyle, { fontWeight: 'bold', marginBottom: 0 }]}>{login && login.result && login.result.name ? login.result.name : 'User'}</Text> : null}
                        {login.loginStatus ? <Text style={Styles.UserNameStyle}>{login && login.result && login.result.phone_no ? login.result.phone_no : ''}</Text> : null}
                    </View>
                    <ScrollView style={{ flex: 1 }}>
                        {ArrayJson.SideMenuTabs && ArrayJson.SideMenuTabs.length > 0 ?
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                showsHorizontalScrollIndicator={false}
                                style={{ width: '100%' }}
                                data={ArrayJson.SideMenuTabs}
                                extraData={this.state}
                                keyExtractor={(item, index) => String(index)}
                                renderItem={this.renderItem}
                            />
                            : <NoDataFound
                                language={language}
                            />
                        }
                    </ScrollView>
                </View>
                {/* {login.loginStatus ? <TouchableNativeFeedback style={Styles.LogoutView} onPress={() => {
                    this.props.logout(); this.props.navigation.navigate('Home')
                }}>
                    <Text style={Styles.SideMenuLogoutTextStyle}>{TextJson.Logout[language.languagecode]}</Text>
                    <Text style={[Styles.SideMenuLogoutTextStyle, { fontSize: ValueFontSize / 1.2 }]}>{version}</Text>
                </TouchableNativeFeedback> : null} */}
            </View>
        )
    }
}

export default connect(loginprops, IncrDecr)(SideMenu);