import React from 'react';
import {View, Text} from 'react-native';
import { Styles } from '../../Utils/Styles';
import TextJson from '../../Utils/TextJson';

class NoDataFound extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
      const { language } = this.props
    return (
      <View style={Styles.NoDataFoundViewStyle}>
        <Text style={Styles.NoDataFoundTextStyle}>{TextJson.NoDataFound[language.languagecode]}</Text>
      </View>
    )
  }
}

export default NoDataFound;