import React from 'react';
import { Text, Dimensions, View } from 'react-native';
import { Styles,Margin } from '../../Utils/Styles';
import Colors from '../../Utils/Colors';

const { width, height } = Dimensions.get('window')

class LoginNote extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { Content, HideColor } = this.props;
    return (
        <View style={[Styles.LoginNoteStyle,HideColor ? {backgroundColor:'transparent'}:null]}>
        <View style={{backgroundColor:HideColor ? 'tranparent' : Colors.LightBlue}}>
          <Text style={Styles.OTPDescription}>{Content}</Text>
        </View>
        </View>
    )
  }
}

export default LoginNote;