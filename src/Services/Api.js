import Axios from "axios";

//Get
export const GetApi = (Api, props) => {
    var authOptions = {
        method: 'GET',
        url: Api,
        headers: {}
    };
    console.log('authOptions ', Api, ' :-  ', authOptions)
    return Axios(authOptions)
        .then(response => {
            console.log("Response ", Api, " :-  ", response)
            return response.data;
        })
        .catch((error) => {
            console.log('Error ', Api, ' :-  ', error)
            handleErrors(error);
        });
}

//PUT
export const PutApi = (Api, props) => {
    var authOptions = {
        method: 'PUT',
        url: Api,
        headers: {
            'accept': '*/*',
        }
    };
    console.log('authOptions ', Api, ' :-  ', authOptions)
    return Axios(authOptions)
        .then(response => {
            console.log("Response ", Api, " :-  ", response)
            return response.data;
        })
        .catch((error) => {
            console.log('Error ', Api, ' :-  ', error)
            handleErrors(error);
        });
}

//Post
export const PostApi = async (Api, Body) => {
    var authOptions = {
        method: 'POST',
        url: Api,
        data: Body,
        headers: {
            'accept': 'application/json',
            'content-type': 'application/json',
        }
    };
    console.log('authOptions ', Api, ' :-  ', authOptions)
    return Axios(authOptions)
        .then(response => {
            console.log("Response ", Api, " :-  ", response.data)
            return response.data;
        })
        .catch((error) => {
            console.log('Error ', Api, ' :-  ', error)
            handleErrors(error);
        });
}























function handleErrors(error) {
    console.log("Error", error)
    if (error && error.message.includes('Network')) {
        alert("Please check network connection.")
    }
    return error;
}